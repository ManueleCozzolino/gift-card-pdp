'use strict';
'use strict';

module.exports = function(grunt) {

    /* Configure
    ============================ */
    var configs = {

        css_combine_files: [
            'src/css/*'
        ],

        js_combine_files: [
            'src/js/*',
        ],

        js_hint_files: [
            'src/js/main.js'
        ],

        watch_files: [
            'src/less/*',
            'src/js/*',
            'src/require_modules/*',
        ]
    }

    /* Init
    ============================ */
    grunt.initConfig({
        less: {
            production: {
                files: {
                    "src/css/noProduction.css": "src/less/noProduction.less",
                    "src/css/main.css": "src/less/main.less",
                    "src/css/_giftcard-front.css": "src/less/_giftcard-front.less",
                    "src/css/_custom-radio.css": "src/less/_custom-radio.less",
                    "src/css/_giftcard-radio.css": "src/less/_giftcard-radio.less",
                    "src/css/_section-header.css": "src/less/_section-header.less",
                    "src/css/_custom-button.css": "src/less/_custom-button.less",
                    "src/css/_add-giftcard-btn.css": "src/less/_add-giftcard-btn.less",
                    "src/css/_giftcard-pdp.css": "src/less/_giftcard-pdp.less",
                    "src/css/_giftcard-recap.css": "src/less/_giftcard-recap.less",
                    "src/css/_custom-input-text.css": "src/less/_custom-input-text.less",
                    "src/css/_card-email-preview.css": "src/less/_card-email-preview.less",
                    "src/css/_sticky-sidebar.css": "src/less/_sticky-sidebar.less",
                    "src/css/_template-mail.css": "src/less/_template-mail.less",
                }
            }
        },
        concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: configs.js_combine_files,
                dest: 'dist/js/compiled.min.js'
            }
        },
        cssmin: {
            combine: {
                files: {
                    'dist/css/main.min.css': configs.css_combine_files
                }
            }
        },
        watch: {
            src: {
                files: configs.watch_files,
                tasks: ['build']
            }
        }
    });

    // Add plugins
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');

    // Register tasks
    grunt.registerTask('build', ['less', 'cssmin', 'concat']);
    grunt.registerTask('default', ['less', 'cssmin', 'concat']);

    grunt.event.on('watch', function(action, filepath) {
        grunt.log.writeln(filepath + ' has ' + action);
    });

};
