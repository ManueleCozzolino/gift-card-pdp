require.config({
    paths: {
        jquery: '../../bower_components/jquery/dist/jquery.min',
        knockout: '../../bower_components/knockout/dist/knockout',
        // stickySidebar: '../../bower_components/sticky-sidebar/dist/sticky-sidebar.min',
        htmlIncluder: './html-includer',
        customRadio: './custom-radio',
        giftcardPdp: './giftcard-pdp',
        stickySidebar: './sticky-sidebar2',
        // stickySidebar: './sticky-sidebar',
    }
});

require(['htmlIncluder', 'giftcardPdp'], function(htmlIncluder, giftcardPdp){
    htmlIncluder.init();

    setTimeout(function() {
        $(document).trigger('htmlMounted');
    }, 2000);

    $(document).bind('htmlMounted', function(){
        giftcardPdp.init();
    });
});
