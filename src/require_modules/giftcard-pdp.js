define(['jquery', 'knockout', 'customRadio', 'stickySidebar'], function($, ko, customRadio, stickySidebar) {
    var giftcardPdp = {
        init: function() {
            // stickySidebar.init();
            // var sidebar = new StickySidebar('.sticky-sidebar', {
            //     containerSelector: '.giftcard-pdp__page-grid',
            // });
            stickySidebar.init();
            customRadio.init();
            this.initKnockoutBindings();

            $('.c').stick_in_parent();
        },
        initKnockoutBindings: function(){
            var koSelectors = {
                placeholder: '.ko-field__placeholder',
                value: '.ko-field__value',
                koFieldClass: 'ko-field',
            };

            ko.bindingHandlers.textPlaceholder = {
                update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                    var $element = $(element),
                    valueAccessorObj = valueAccessor(),
                    valueUnwrapped = ko.unwrap(valueAccessorObj);

                    if($element.hasClass(koSelectors.koFieldClass)){
                        var $displayedPlaceholder = $element.children(koSelectors.placeholder);
                        var $displayedValue = $element.children(koSelectors.value);

                        $displayedValue.text(valueUnwrapped);
                        if(valueUnwrapped.length){
                            $displayedPlaceholder.addClass('hide');
                        } else {
                            $displayedPlaceholder.removeClass('hide');
                        }
                    }
                }
            };

            ko.bindingHandlers.cardImg = {
                update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                    alert('test');
                }
            };

            var viewModel = {
                headline: ko.observable(''),
                message: ko.observable(''),
                selectedAmount: ko.observable(''),
                cardImg: ko.observable('url(https://i2.wp.com/www.kenlyonsphotography.com.au/wp-content/uploads/2014/10/140409-London-England-195928-1024px.jpg?ssl=1)'),
            };

            ko.applyBindings(viewModel);
        }
    }

    return giftcardPdp;
});
