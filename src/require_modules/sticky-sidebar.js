define(['jquery'], function($) {
    var stickySidebar = {
        elements: {
            sticky: '.sticky-sidebar',
        },
        vars: {
            opt: {
                stuckClass: 'fixed',
                bottomClass: 'bottom',
                container: 'auto',
                offsetTop: 0,
                offsetBottom: 0,
                bottom: true
            }
        },
        init: function(opt) {
            this.vars.opt = opt ? Object.assign({}, this.vars.opt, opt) : this.vars.opt;
            this.bind();

        },
        bind: function() {
            $(window).on('scroll', $.proxy(this.handleScroll, this));
        },
        handleScroll: function() {
            var o = this.vars.opt,
                $s = $(this.elements.sticky),
                $c = (o.container === 'auto') ? $s.parent() : $(o.container),
                sh = $s.outerHeight(),
                originalMarginTop = parseInt($s.css('margin-top'), 10),
                stickyTop = $s.offset().top - o.offsetTop - originalMarginTop,
                stickyBottom = $c.offset().top + $c.outerHeight() - o.offsetTop,
                scroll = $(window).scrollTop();

            var preserveStickyWidth = $c.width();
            $c.css('position', 'relative');
            $s.css('width', preserveStickyWidth);

            if (scroll >= stickyTop) {
                if (((scroll + sh + o.offsetBottom) > stickyBottom) && o.bottom) {
                    $s.removeClass(o.stuckClass);
                    $s.addClass(o.bottomClass);
                } else {
                    $s.css('margin-top', o.offsetTop + originalMarginTop);
                    $s.addClass(o.stuckClass);
                }
            } else {
                $s.css('margin-top', originalMarginTop);
                $s.removeClass(o.stuckClass);
                $s.removeClass(o.bottomClass);
            }
        }
    }
    return stickySidebar;
});
