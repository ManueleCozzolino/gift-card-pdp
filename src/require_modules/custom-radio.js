define(['jquery'], function($) {
    var customRadio = {
        elements: {
            radioIcon: '.custom-radio .custom-radio__icon',
            radioContainer: '.custom-radio',
            actualRadio: '.custom-radio__input',
        },
        init: function() {
            this.bind();
        },
        bind: function() {
            $('body').on('click', this.elements.radioContainer, $.proxy(this.toggleSelected, this));
        },
        toggleSelected: function(e) {
            var $radioGUI = $(e.currentTarget),
                $radio = $radioGUI.find(this.elements.actualRadio),
                radioName = $radio.attr('name'),
                selectedClass = 'custom-radio--selected';

            $(this.elements.actualRadio)
                .filter('[name="' + radioName + '"]')
                .closest(this.elements.radioContainer)
                .not(':hidden')
                .removeClass(selectedClass)
                .find(this.elements.actualRadio)
                .attr('checked', false);

            $radioGUI
                .addClass(selectedClass)
                .find(this.elements.actualRadio)
                .attr('checked', true);
        }
    };
    return customRadio;
});
